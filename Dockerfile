ARG NME=gitpod
ARG HOME=/home/${NME}

FROM docker.io/alpine:edge as alpine-stage

ARG NME
ARG HOME

RUN apk add --no-cache -u alpine-conf alpine-sdk atools autoconf automake bash cmake findutils \
  gdb git-lfs nano openssh-client pax-utils sudo
RUN adduser -u 33333 -s /bin/bash -D ${NME} \
&& addgroup ${NME} abuild && addgroup ${NME} tty \
&& echo gitpod:gitpod | chpasswd \
&& mkdir /workspace ${HOME}/packages ${HOME}/.bashrc.d
COPY profile /home/${NME}/.profile

RUN chown -R ${NME}:${NME} /home/${NME} /workspace \
&& echo "Defaults  lecture=\"never\"" > /etc/sudoers.d/${NME} \
&& echo "${NME} ALL=NOPASSWD : ALL" >> /etc/sudoers.d/${NME} \
&& sed "s/ERROR_CLEANUP.*/ERROR_CLEANUP=\"\"/" -i /etc/abuild.conf

#########################
FROM gitpod/workspace-base

ARG NME
USER root

COPY aroot.sh /usr/local/bin/aroot
COPY uroot.sh /usr/local/bin/uroot
RUN mkdir /alpine
COPY --from=alpine-stage / /alpine/
RUN rm -rf /alpine/dev && mkdir /alpine/dev && touch /alpine/dev/null

COPY bashrc /tmp/bashrc
# add alias alroot to enter alpine chroot and start in alpine chroot
RUN su ${NME} -c "cat /tmp/bashrc >> ~/.bashrc" \
&& rm /tmp/bashrc

USER ${NME}
