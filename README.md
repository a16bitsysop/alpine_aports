# alpine_gitpod

Repo for alpine_gitpod docker image with Gitpod integration. 

## Getting started

When gitpod is launched it will update the alpine chroot, and start in the chroot with workspace mounted in /workspace.  Which is the same location as the gitpod base image.

Alpine linux is installed in the /alpine folder.

The `aroot` command runs a command as root in /alpine chroot (or changes to it if no command is given).
The `uroot` command runs a command as gitpod user in the chroot.

The `alroot` command returns to the alpine chroot after an exit as the gitpod user.

